import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subiectul2  extends JFrame {
    JTextField text1;
    JTextField text2;
    JTextField text3;
    JButton button;

    Subiectul2(){
        setTitle("Subiectul 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,200);
        setVisible(true);
    }

    public void init(){
        setLayout(new GridLayout(3,3));

        text1=new JTextField();
        text2=new JTextField();
        text3=new JTextField();
        text3.setEditable(false);


        button=new JButton("Aduna");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
               int n1=Integer.parseInt(text1.getText());
               int n2=Integer.parseInt(text2.getText());
               int n3=n1+n2;
               text3.setText(String.valueOf(n3));

            }
        });

        add(text1); add(text2); add(text3); add(button);
    }

    public static void main(String[] args) {
        new Subiectul2();
    }
}
