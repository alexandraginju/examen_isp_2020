import java.util.ArrayList;
import java.util.List;

public class Subiectul1 {
    interface  C{
    }

    class B{
        private long t;
        private List<D> d=new ArrayList<D>();
        private E e;

        public void x(){}
    }

    class D{
    }

    class A{
        public void met(B b){}
    }

    class E implements C{
        F f;

        public void metG(int i){}
    }

    class F{
        public void metA(){}
    }
}





